# topquadrant data platform in vagrant

## introduction

this repo is a wrapper around the ansible installation of topquadrant's data platform

# installation notes

## add installation files
Download the file from topquadrant and put it into the bin/topbraid folder
- https://download.topquadrant.com/live/edg/
- https://download.topquadrant.com/dp/

## build infrastructure
```
git clone --recursive https://gitlab.com/btotr/topbraid-edg-vagrant.git
cd topbraid-edg-vagrant 
vagrant up
```

## provision nfs
```
vagrant ssh provisioner 
cd provisioner
ansible-playbook nfs.yml -i ./hosts.example
```

## mount nfs folder into provisioner folder
```
ansible-playbook playbooks/create-and-mount-release-folder.yml -i ./hosts.example`
```

## provision the data platform
```
ansible-playbook proxy.yml -i ./hosts.example
ansible-playbook rdfdelta.yml -i ./hosts.example
ansible-playbook topbraid.yml -i ./hosts.example
```

## usage
open you webrowser with the following url: `http://10.0.0.11` 

## optional: sync with ansible master 
```
git config --file=.gitmodules submodule.Submod.url https://gitlab.com/btotr/topbraid-edg-vagrant.git
git config --file=.gitmodules submodule.Submod.branch master
git submodule sync
git submodule update --init --recursive --remote
```