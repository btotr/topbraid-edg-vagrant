$scriptUbuntu = <<-SCRIPT
echo installing ansible
apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
apt-get update
apt-get --assume-yes install duplicity python-pip
pip install paramiko
apt-get --assume-yes install ansible sshpass
SCRIPT

$scriptCentos = <<-SCRIPT
    dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y
    dnf install ansible
    ansible-galaxy install geerlingguy.nfs

SCRIPT

Vagrant.configure(2) do |config|

  config.vm.box = "bento/centos-8"
  config.ssh.insert_key = false
  config.vm.provision "file", source: "keys/vagrant", destination: "/home/vagrant/.ssh/id_rsa"
  config.vm.provision "file", source: "keys/vagrant.pub", destination: "/home/vagrant/.ssh/id_rsa.pub"
  config.vm.provision "shell" do |s|
    ssh_pub_key = File.readlines("keys/vagrant.pub").first.strip
    s.inline = <<-SHELL
      chown vagrant /home/vagrant/.ssh/id_rsa
      chown vagrant /home/vagrant/.ssh/id_rsa.pub
      chmod 400 /home/vagrant/.ssh/id_rsa
      echo #{ssh_pub_key} >> /home/vagrant/.ssh/authorized_keys
    SHELL
  end
  
  config.vm.define :provisioner do |pv|
    pv.vm.hostname = "provisioner"
    pv.vm.network :private_network, ip: '10.0.0.5'
    pv.vm.synced_folder "provisioning/", "/home/vagrant/provisioning", mount_options: ["dmode=775"]
    pv.vm.provision "shell", inline: $scriptCentos
  end

  config.vm.define :rdfdelta do |d|
    d.vm.hostname = "rdfdelta"
    d.vm.network :private_network, ip: '10.0.0.2'
  end

  config.vm.define :nfs do |n|
    n.vm.hostname = "nfs"
    n.vm.network :private_network, ip: '10.0.0.3'
    n.vm.synced_folder "bin/", "/home/vagrant/bin", mount_options: ["dmode=775"]
    n.vm.provision "shell" do |ns|
      ns.inline = <<-SHELL
        mkdir /home/vagrant/topquadrant
        mkdir /home/vagrant/db
        cp -r /home/vagrant/bin/* /home/vagrant/topquadrant/ 
      SHELL
    end
  end

  config.vm.define :bastion do |b|
    b.vm.hostname = "bastion"
    b.vm.network :private_network, ip: '10.0.0.4'
  end

  (1..2).each do |i|
    config.vm.define "edg-#{i}" do |node|
      node.vm.network :private_network, ip: "10.0.0.#{i+10}"
	    node.vm.provider "virtualbox" do |v|
		    v.memory = "1024"
     	  v.cpus = 2
  	  end
    end
  end
end